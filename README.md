# But
Déployer une API python avec Redis via Helm sur Kubernetes.

## Commandes Minikube utiles
```
minikube start --kubernetes-version=1.20.0 --drivernone
minikube start --driver=docker
minikube kubectl -- get pods -A
minikube addons enable metrics-server
minikube kubectl create loann
minikube kubectl delete loann
minikube kubectl -- get service ou svc
minikube kubectl -- get namespace ou ns
minikube kubectl -- get replicaset ou rs
minikube kubectl -- get deployment ou deploy
minikube kubectl -- get nodes ou no
minikube kubectl -- get all -n loann
```
## Commandes Helm utiles
```
helm create <chartname>
helm lint <chartname>
helm list -A
helm install redisinsight redisin_loann/ -n loann --> redisinsight est le nom de la release/application
helm list -A --> redisinsight is now listed 
helm upgrade redisinsight redisin_loann/ -n loann
helm uninstall redisinsight --> redisinsight now disappeared from list of deployed apps
helm repo list
helm repo add "..."
echo 'eval "$(helm completion bash)"' >> ~/.bashrc
echo "$(helm completion bash)" >> ~/.bashrc --> marche aussi je crois
source ~/.bashrc
helm list -A --> liste toutes les releases
helm uninstall redisinsight
helm uninstall redisinsight -n loann
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install my-ingress-controller ingress-nginx/ingress-nginx
minikube kubectl -- get po -n loann
minikube kubectl -- get ingress -n loann
api_loann/ et redis_loann/ posent problème lors du déploiement avec Helm avec le même message concernant le nom des volumes dans deployment.yaml
minikube kubectl -- top nodes/pods
```
