# Description projet:
- Création de charts (FastAPI python, redis et redisinsight) Helm - Kubernetes Packet Manager - pour déployer sur un cluster Kubernetes provisionné en utilisant Hashicorp Vagrant (cf https://gitlab.com/xavki/vagrant-files/-/tree/master/auto-kub?ref_type=heads)
- docker-compose.yaml sert uniquement à tester en local, le fonctionnement de notre API lors du dév de main.py
- 6379: Redis
- 8001: Redisinsight
- 8080: FastAPI

# Installation :
- py --version
- pip --version
- pip3 install --upgrade pip
- pip3 install virtualenv OU 
- py -m venv liveki07
- pip3 install PyYAML
- pip3 install uvicorn : https://fastapi.tiangolo.com/tutorial/first-steps/
- uvicorn main:app --reload
- --reload: make the server restart after code changes. Only use for development.

- XML and JSON are the only ones that Python supports out of the box. For yaml : "import yaml"

- Note: Before version 6.0 of the PyYAML library, the default way of parsing YAML documents had always been the yaml.load() function, which defaulted to using an unsafe parser. With the latest release, you can still use this function, but it requires you to explicitly specify a particular loader class as a second parameter : yaml_setting = yaml.load(ymlfile, Loader = yaml.Loader
