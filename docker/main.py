# importe spécifiquement la classe FastAPI du module fastapi, import fastapi importerait tout le module
# évite d'écrire app = fastapi.FastAPI()
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from pydantic import BaseSettings, BaseModel
from redis import Redis
from typing import Optional
import yaml
import os, sys, random, time, uvicorn, pickle


seconds = [0.5, 1, 2.5, 3.5]

# Pour versionner l'API par le path
api_path = "/api/v1/"
yaml_setting = dict()

local_path = os.path.abspath(os.path.dirname(__file__))

if os.path.isfile(os.path.join(local_path,"config.yaml")):
    with open(os.path.join(local_path,"config.yaml"), "r") as ymlfile:
        yaml_setting = yaml.load(ymlfile, Loader = yaml.Loader)

    redis_host     = yaml_setting["redis_host"]
    redis_password = yaml_setting["redis_password"]
    api_port       = yaml_setting["api_port"]
    api_listen     = yaml_setting["api_listen"]

elif os.getenv("REDIS_HOST") and os.getenv("REDIS_PASSWORD"):
    redis_host     = os.getenv("REDIS_HOST")
    redis_password = os.getenv("REDIS_PASSWORD")
    api_port       = 8080
    api_listen     = "127.0.0.1"

else:
    print("Erreur - config.yaml et les variables d'environnement n'existent pas")
    sys.exit(1)


class Config(BaseSettings):
    redis_password: str = redis_password
    redis_host: str = redis_host
    api_port: int = api_port
    api_listen: str = api_listen


# je pourrais préciser l'optionnalité
class Person(BaseModel):
    name: str
    age: int
    city: str


app = FastAPI()
config = Config()

redis = Redis(
    password = config.redis_password,
    host = config.redis_host,
    db = 0,
    socket_timeout = 5.0,
    charset = "UTF-8"
)


@app.get(api_path + "health")
def health():
    return jsonable_encoder({"message": "L'api est fonctionnelle!"})


# Route simple de test
@app.get(api_path + "delay")
def delay():
    delay = random.choice(seconds)
    time.sleep(delay)
    return jsonable_encoder({"delay": str(delay) + "secondes"})


@app.get(api_path + "list")
def list():
    response = []
    for key in redis.scan_iter():
        # Je "déserialize" ou décode les bits récupérés
        response.append(key.decode("UTF-8"))
    
    return jsonable_encoder(response)


@app.get(api_path + "get/{name}")
def getPerson(name):
    p_data = redis.get(name)
    if p_data:
        response = pickle.loads(p_data)
    else:
        response = {"Message:": "Utilisateur inconnu."}
    
    return jsonable_encoder(response)


@app.get(api_path + "clear")
def clear():
    count = 0
    for key in redis.scan_iter():
        redis.delete(key)
        count += 1

    response = {"Elements deleted:" : count}

    return jsonable_encoder(response)


# @app.put() possible également
@app.post(api_path + "insert")
def insert(person: Person):
    data = {"name": person.name, "city": person.city, "age": person.age}
    # Je "serialize" avec pickle
    p_data = pickle.dumps(data)

    redis.set(person.name, p_data)

    response = {"Nouvel utilisateur:": person.name}

    return jsonable_encoder(response)


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host = api_listen,
        port = api_port,
        log_level = "info"
    )

